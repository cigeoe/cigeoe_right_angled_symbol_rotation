# -*- coding: utf-8 -*-

#from PyQt5.QtCore import * 
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QMessageBox
from qgis.gui import *
from qgis.core import *
#from qgis.gui import QgsMessageBar, QgsMapToolEmitPoint, QgsRubberBand, QgsVertexMarker
#from qgis.core import QgsWkbTypes, QgsPointXY, QgsPoint, QgsRectangle, QgsFeatureRequest, QgsMapLayer, Qgis
from qgis.utils import iface 

from math import *
import math


class drawBox(QgsMapToolEmitPoint): 
           
    def __init__(self, canvas, iface):
        """
        draws a rectangle, then features that intersect this rectangle are added to selection 
        """
        # Store reference to the map canvas
        self.canvas = canvas  

        self.iface = iface        # add iface for work with active layer                
                
        QgsMapToolEmitPoint.__init__(self, self.canvas)
                
        self.rubberBand = QgsRubberBand(self.canvas, geometryType=QgsWkbTypes.LineGeometry)
        self.rubberBand.setColor(QColor(0, 0, 240, 100))
        self.rubberBand.setWidth(1)
        
        self.reset() 
               

    def reset(self):
        self.startPoint = self.endPoint = None
        self.isEmittingPoint = False
        
        self.rubberBand.reset( QgsWkbTypes.PolygonGeometry )


    def canvasPressEvent(self, e):
        """
        Method used to build rectangle if shift is held, otherwise, feature select/deselect and identify is done.
        """
        self.startPoint = self.toMapCoordinates(e.pos())
        self.endPoint = self.startPoint
        self.isEmittingPoint = True
        
        self.showRect(self.startPoint, self.endPoint)
        
    #self.isEmittingPoint is used to build the rubberband on the method on CanvasMoveEvent
            
    def canvasMoveEvent(self, e):
        """
        Used only on rectangle select.
        """
        if not self.isEmittingPoint:
            return
        self.endPoint = self.toMapCoordinates( e.pos() )
        self.showRect(self.startPoint, self.endPoint)

   
    def showRect(self, startPoint, endPoint):
        """
        Builds rubberband rect.
        """
        
        self.rubberBand.reset( QgsWkbTypes.PolygonGeometry )
        if startPoint.x() == endPoint.x() or startPoint.y() == endPoint.y():
            return
        
        point1 = QgsPointXY(startPoint.x(), startPoint.y())
        point2 = QgsPointXY(startPoint.x(), endPoint.y())
        point3 = QgsPointXY(endPoint.x(), endPoint.y())
        point4 = QgsPointXY(endPoint.x(), startPoint.y())

        self.rubberBand.addPoint(point1, False)
        self.rubberBand.addPoint(point2, False)
        self.rubberBand.addPoint(point3, False)
        self.rubberBand.addPoint(point4, True)    # true to update canvas
        self.rubberBand.show()


    def rectangle(self):
        """
        Builds rectangle from self.startPoint and self.endPoint
        """
        if self.startPoint is None or self.endPoint is None:
            return None
        elif self.startPoint.x() == self.endPoint.x() or self.startPoint.y() == self.endPoint.y():
            return None
        return QgsRectangle(self.startPoint, self.endPoint)

    
    def canvasReleaseEvent(self, e):
        """
        After the rectangle is built, here features are selected.
        """
        self.isEmittingPoint = False

        rect = self.rectangle()            #QgsRectangle
        
        if rect is not None:           
           
            featLine = []
            featPoint = []
            totfeat = 0

            #***************************************************************** 
            layers = self.iface.mapCanvas().layers() 
            
            for layer in layers:                 #iterate all layers                

                r = self.canvas.mapSettings().mapToLayerCoordinates(layer, rect)   
                #for feat in layer.getFeatures(QgsFeatureRequest(r)): 
                request = QgsFeatureRequest(r)  
                for feat in layer.getFeatures(request):                    
                    if feat.geometry().type()==QgsWkbTypes.LineGeometry:
                        layerLine = layer
                        totfeat += 1
                        featLine.append(feat) 
                    
                    if feat.geometry().type()==QgsWkbTypes.PointGeometry:
                        layerSymbol = layer
                        totfeat += 1
                        featPoint.append(feat)                     
            
            
            if totfeat != 2:
                QMessageBox.information(self.iface.mainWindow(), "Error", 'Invalid features selected. There must be exactly one point and one line in the square.' )   
                self.rubberBand.hide()          
                return  
            if (len(featLine)) != 1:
                QMessageBox.information(self.iface.mainWindow(), "Error", 'Invalid features selected. There must be exactly one point and one line in the square.' )  
                self.rubberBand.hide()           
                return
            if (len(featPoint)) != 1:
                QMessageBox.information(self.iface.mainWindow(), "Error", 'Invalid features selected. There must be exactly one point and one line in the square.' )    
                self.rubberBand.hide()         
                return
                         
            #----------------------------------------------------
            if not layerSymbol.isEditable():
                QMessageBox.information(self.iface.mainWindow(), "Error", 'Point layer is not editable.')
                self.rubberBand.hide()
                return  
            
            geomx= featPoint[0]              
            point0 = geomx.geometry().asPoint()  
            point1 = QgsFeature()
            point1.setGeometry(QgsGeometry.fromPointXY(point0)) 

            geom = featLine[0]              
            line0 = geom.geometry().asPolyline()   
            pointsLine=[]                           
            for i in range(len(line0)):
                pointsLine.append(line0[i])               # list of polyline points (x,y)

            #  splits line into segments 
            segLine = self.segmentLine(pointsLine)   
                       
            segmentOK = []
            for ind in range(len(segLine)):                             
                xxxLine1 = []   
                xxxLine1.append(segLine[ind][0])
                xxxLine1.append(segLine[ind][1])

                segmentLine1 = QgsFeature()                
                segmentLine1.setGeometry(QgsGeometry.fromPolylineXY(xxxLine1))           # Qgsgeometry: LineString
                                
                #buff = segmentLine1.geometry().buffer(10, -1)
                buff = segmentLine1.geometry().buffer(1, -1)       # buffer( metros, -1)
                if buff.intersects(point1.geometry()):
                    segmentOK.append(xxxLine1) 
                    break            

            if len(segmentOK) != 1:
                QMessageBox.information(self.iface.mainWindow(), "Error", 'No point found in a buffer of 1m from the line' )    
                self.rubberBand.hide()    
                return
                        
                                    
            
            angleIndex=-1
            for attIdx in range(len(layerSymbol.attributeList())):
                if layerSymbol.attributeDisplayName(attIdx) and layerSymbol.attributeDisplayName(attIdx).lower()=="angle":
                    angleIndex=attIdx                    
                    break

            if angleIndex ==-1:
                QMessageBox.information(self.iface.mainWindow(), "Error", 'Angle attribute not found in point layer.' )    
                self.rubberBand.hide()    
                return

            #----------------------------------------------
            # calcular angulo   
            
            x1 = segmentOK[0][0][0] 
            y1 = segmentOK[0][0][1]
            x2 = segmentOK[0][1][0] 
            y2 = segmentOK[0][1][1]   
             
            x = x2 - x1
            y = y2 - y1
            val = math.atan2(x,y)         
            val1 = math.degrees(val)

            if val1 < 0:
                val1 = val1 + 180 

            anglex = val1 + 90                
            anglex1 = round(anglex, 2)

            #----------------------------------------------          
    
            angleIni =featPoint[0][angleIndex]  
            
            angleIni1 = round(angleIni, 2)
                       
            if angleIni1 ==  anglex1:                
                if angleIni1 > 180:
                    anglex = anglex - (180)
                else:
                    anglex = anglex + (180)

            #---------------------------------
            
            featPoint[0][angleIndex]= anglex                  #ângulo, graus, a definir
            layerSymbol.updateFeature(featPoint[0])
            layerSymbol.triggerRepaint()

            self.rubberBand.hide()
                            
            self.iface.messageBar().pushMessage("CIGeoE Right Angled Symbol Rotation", "Right Angled Symbol Rotation done" , level=Qgis.Info, duration=2)
           
     


    
    #----------------------------------           
    
    def segmentLine(self, lista):  
        pointseg = []
        seg = []                
        npoint = 0
        for point in lista:  
            npoint += 1    
            if npoint == 2: 
                seg.append(point)
                pointseg.append(seg)
                seg = []
                npoint = 1                
            seg.append(point) 
        return pointseg  
        
    #---------------------------------