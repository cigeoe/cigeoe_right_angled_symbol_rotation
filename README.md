Right Angled Symbol Rotation

# Installation

For QGIS 3:

There are two methods to install the plugin:

- Method 1:

  1- Copy the folder “cigeoe_right_angled_symbol_rotation” ” to folder:

  ```bash
  [drive]:\Users\[user]\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins
  ```

  2 - Start QGis and go to menu "Plugins" and select "Manage and Install Plugins"

  3 - Select “CIGeoE Right Angled Symbol Rotation”

  4 - After confirm the operation the plugin will be available in toolbar

- Method 2:

  1 - Compress the folder “cigeoe_right_angled_symbol_rotation” (.zip).

  2 - Start QGis and go to "Plugins" and select “Manage and Install Plugins”

  3 - Select “Install from Zip”, choose .zip generated in point 1) and then select "Install Plugin"

# Usage

1 - In the “Layers Panel”, at least two layers must be visible. One of them must contain “point geometries” and the other, “linestring geometries”. Edit the “point geometries” layer. Click on the plugin icon.

![ALT](/images/image1.png)

2 - When you release the mouse button, the point symbol is rotated so that it is 90º to one side of the line. If you want it to be on the other side of the line, simply draw a new rectangle over the point and line, that is, once perpendicular to the straight line, repeating the process, the symbol will rotate 180º.

![ALT](/images/image2.png)

3 - To deactivate, click on the plugin icon.

![ALT](/images/image3.png)

# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

# License

[AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)
